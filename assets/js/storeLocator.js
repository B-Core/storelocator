var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPod|iPad/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};

function StoreLocator(params) {

    console.log('StoreLocator:params', params);

    var csv, magasins, map, marker, direction, center, pos, magasin;
    bounds = new google.maps.LatLngBounds(),
        geocoder = new google.maps.Geocoder(),
        infowindow = new google.maps.InfoWindow(),
        directionsDisplay = new google.maps.DirectionsRenderer({
            suppressMarkers: true
        }),
        directionsService = new google.maps.DirectionsService(),
        locationAllowed = false;

    var ebRand = Math.random() + '';
    ebRand = ebRand * 1000000;

    var coordsOrd = {};

    var Rm = 3961; // mean radius of the earth (miles) at 39 degrees from the equator
    var Rk = 6373; // mean radius of the earth (km) at 39 degrees from the equator


    getUserPosition(function(coords) {
        if (coords) {
            displayMap(coords);
            _s4mq.push(['trackAction', {
                name: "OK_LOCATION"
            }]);
        } else {
            _s4mq.push(['trackAction', {
                name: "NO_LOCATION"
            }]);
            var cta1, cta2;

            cta1 = document.getElementById("CTA_1");
            cta2 = document.getElementById("CTA_2");

            cta1.style.display = "block";
            cta2.style.display = "block";
        }
    });

    function initializeMap() {

        console.log('initializeMap');


        direction = new google.maps.DirectionsRenderer({
            map: map
        });

        var mapOptions = params && params.mapOptions ? params.mapOptions : {
            zoom: 17,
            zoomControl: true,
            zoomControlOptions: {
                style: google.maps.ZoomControlStyle.SMALL,
            },
            disableDoubleClickZoom: true,
            mapTypeControl: false,
            scaleControl: false,
            scrollwheel: true,
            panControl: false,
            streetViewControl: true,
            draggable: true,
            overviewMapControl: false,
            overviewMapControlOptions: {
                opened: false,
            },
            center: {
                lat: 48.85661,
                lng: 2.35222
            },
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            // styles: [{
            //     "featureType": "landscape.man_made",
            //     "elementType": "geometry",
            //     "stylers": [{
            //         "color": "#c3b0b1"
            //     }]
            // }, {
            //     "featureType": "landscape.natural",
            //     "elementType": "geometry",
            //     "stylers": [{
            //         "color": "#d0e3b4"
            //     }]
            // }, {
            //     "featureType": "landscape.natural.terrain",
            //     "elementType": "geometry"
            // }, {
            //     "featureType": "poi",
            //     "elementType": "labels"
            // }, {
            //     "featureType": "poi.business",
            //     "elementType": "all",
            //     "stylers": [{
            //         "visibility": "off"
            //     }]
            // }, {
            //     "featureType": "poi.medical",
            //     "elementType": "geometry",
            //     "stylers": [{
            //         "color": "#fbd3da"
            //     }]
            // }, {
            //     "featureType": "poi.park",
            //     "elementType": "geometry",
            //     "stylers": [{
            //         "color": "#bde6ab"
            //     }]
            // }, {
            //     "featureType": "road",
            //     "elementType": "geometry.stroke"
            // }, {
            //     "featureType": "road",
            //     "elementType": "labels"
            // }, {
            //     "featureType": "road.highway",
            //     "elementType": "geometry.fill",
            //     "stylers": [{
            //         "color": "#ffe15f"
            //     }]
            // }, {
            //     "featureType": "road.highway",
            //     "elementType": "geometry.stroke",
            //     "stylers": [{
            //         "color": "#efd151"
            //     }]
            // }, {
            //     "featureType": "road.arterial",
            //     "elementType": "geometry.fill",
            //     "stylers": [{
            //         "color": "#ffffff"
            //     }]
            // }, {
            //     "featureType": "road.local",
            //     "elementType": "geometry.fill",
            //     "stylers": [{
            //         "color": "black"
            //     }]
            // }, {
            //     "featureType": "transit.station.airport",
            //     "elementType": "geometry.fill",
            //     "stylers": [{
            //         "color": "#cfb2db"
            //     }]
            // }, {
            //     "featureType": "water",
            //     "elementType": "geometry",
            //     "stylers": [{
            //         "color": "#a2daf2"
            //     }]
            // }],
        }

        map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

        google.maps.event.addListenerOnce(map, 'idle', function() {
            directionsDisplay.setMap(map);
            document.getElementById('acf-loader').style.display = 'none';
        });

    }

    // Getting user Position 
    function getUserPosition(callback) {

        _s4mq.push(['trackAction', {
            name: "ALLOW_LOCATION"
        }]);

        console.log('getUserPosition');

        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(
                function(position) {
                    callback(position.coords);
                },
                function() {
                    callback();
                }, {
                    enableHighAccuracy: true,
                    timeout: 6000,
                    maximumAge: 0
                }
            );
        } else {
            // Browser doesn't support Geolocation
            callback();
        }
    };

    var setStores = params && params.setStores ? params.setStores : [];

    return {
        initializeMap: initializeMap
    }

    /* main function */
    function coordToKm(cfg) {
        var t1, n1, t2, n2, lat1, lon1, lat2, lon2, dlat, dlon, a, c, dm, dk, mi, km;

        // get values for lat1, lon1, lat2, and lon2
        t1 = cfg.from.lat;
        n1 = cfg.from.lng;
        t2 = cfg.to.lat;
        n2 = cfg.to.lng;

        // convert coordinates to radians
        lat1 = deg2rad(t1);
        lon1 = deg2rad(n1);
        lat2 = deg2rad(t2);
        lon2 = deg2rad(n2);

        // find the differences between the coordinates
        dlat = lat2 - lat1;
        dlon = lon2 - lon1;

        // here's the heavy lifting
        a = Math.pow(Math.sin(dlat / 2), 2) + Math.cos(lat1) * Math.cos(lat2) * Math.pow(Math.sin(dlon / 2), 2);
        c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a)); // great circle distance in radians
        dm = c * Rm; // great circle distance in miles
        dk = c * Rk; // great circle distance in km

        // round the results down to the nearest 1/1000
        mi = round(dm);
        km = round(dk);


        return km;
    }

    // convert degrees to radians
    function deg2rad(deg) {
        rad = deg * Math.PI / 180; // radians = degrees * pi/180
        return rad;
    }

    // round to the nearest 1/1000
    function round(x) {
        return Math.round(x * 1000) / 1000;
    }

    function nearestMag(coords) {
        for (var i = 0; i < setStores.length; i++) {
            var distance = coordToKm({
                from: {
                    lat: coords.A,
                    lng: coords.F
                },
                to: {
                    lat: setStores[i][5],
                    lng: setStores[i][6]
                }
            });
            coordsOrd[distance] = setStores[i];
        }

        var coordsKeys = [];

        for (k in coordsOrd) {
            if (coordsOrd.hasOwnProperty(k)) {
                coordsKeys.push(k);
            }
        }

        coordsKeys.sort(function(a, b) {
            return a - b
        });

        var distance = coordsKeys[0];

        var magasin = coordsOrd[distance];

        console.log(magasin);

        if (coords) {
            onFound(magasin);
        }

        // displayNearestAddress(magasin);
    }

    function displayMap(coords) {

        var mapScreen = document.getElementById('mapScreen');

        mapScreen.style.display = "block";

        pos = new google.maps.LatLng(coords.latitude, coords.longitude);

        initializeMap();
        nearestMag(pos);
    }


    function onFound(coordMag) {

        console.log("onFound:coordMag :", coordMag[5])

        if (pos) {

            magasin = new google.maps.LatLng(parseFloat(coordMag[5]), parseFloat(coordMag[6]));

            var userPos = pos;

            var userMarker = new google.maps.Marker({
                position: userPos,
                map: map
            });

            var request = {
                origin: userPos,
                destination: magasin,
                travelMode: google.maps.TravelMode["DRIVING"]
            };

            directionsService.route(request, function(response, status) {
                if (status == google.maps.DirectionsStatus.OK) {
                    directionsDisplay.setDirections(response);
                }
            });

            codeLatLng(magasin.A, magasin.F, coordMag);

            var mapCanvas = document.getElementById('map-canvas');

            mapCanvas.addEventListener("click", redirectToMap, false);

        }

    }

    function codeLatLng(lat, lng, coordStore) {
        var lat = lat;
        var lng = lng;
        var latlng = new google.maps.LatLng(lat, lng);
        geocoder.geocode({
            'latLng': latlng
        }, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                if (results[1]) {
                    var image = 'assets/img/mag_icon.png';
                    var magMarker = new google.maps.Marker({
                        position: magasin,
                        map: map,
                        icon: image
                    });
                    var infowindow = new google.maps.InfoWindow({
                        map: map,
                        content: '<p><img src="assets/img/logo.png" id="logo-infobulle"/></p>' +
                            '<p id="text-infobulle">' + coordStore[4] + '<br>' + coordStore[2] + ' ' + coordStore[3] + '</p><br>'
                    });

                    infowindow.open(map, magMarker);

                    map.setCenter(magMarker.getPosition());
                } else {
                    console.log('No results found');
                }
            }
        });
    };

    function redirectToMap() {
        _s4mq.push(['trackAction', {
            name: "CLICK_MAP"
        }]);
        goToMap(magasin);
    };

    function goToMap(mag) {
        var address;
        if (isMobile.iOS()) {
            address = 'http://maps.apple.com/?q=' + mag.A + ',' + mag.F;
            location.href = address;
        } else {
            address = 'http://maps.google.com/?q=' + mag.A + ',' + mag.F;
            document.location.href = address;
            location.href = address;
        }
    };


}
